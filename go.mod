module gitlab.com/gitlab-org/ci-cd/runner-release-helper

go 1.12

require (
	github.com/golang/protobuf v1.3.4 // indirect
	github.com/hashicorp/go-version v1.0.0
	github.com/jstemmer/go-junit-report v0.9.1
	github.com/mitchellh/gox v1.0.1
	github.com/shuLhan/go-bindata v3.4.0+incompatible
	github.com/sirupsen/logrus v1.3.0
	github.com/stretchr/testify v1.4.0
	github.com/urfave/cli v1.20.0
	github.com/xanzy/go-gitlab v0.27.0
	gitlab.com/ayufan/golang-cli-helpers v0.0.0-20171103152739-a7cf72d604cd
	golang.org/x/net v0.0.0-20200301022130-244492dfa37a // indirect
	golang.org/x/oauth2 v0.0.0-20200107190931-bf48bf16ab8d // indirect
	google.golang.org/appengine v1.6.5 // indirect
	gopkg.in/yaml.v2 v2.2.2
)
