package gitlab

import (
	"fmt"

	"github.com/xanzy/go-gitlab"
)

type DefaultClient struct {
	client *gitlab.Client
}

func (c *DefaultClient) init(baseURL, token string) error {
	c.client = gitlab.NewClient(nil, token)

	err := c.client.SetBaseURL(baseURL)
	if err != nil {
		return fmt.Errorf("invalid baseURL: %v", err)
	}

	return nil
}

func (c *DefaultClient) CreateBranch(params CreateBranchParams) (Branch, error) {
	project, err := c.findProject(params.ProjectID)
	if err != nil {
		return Branch{}, err
	}

	options := &gitlab.CreateBranchOptions{
		Branch: &params.Name,
		Ref:    &params.Ref,
	}

	rawBranch, _, err := c.client.Branches.CreateBranch(project.ID, options)
	if err != nil {
		return Branch{}, fmt.Errorf("error while creating branch %q: %v", params.Name, err)
	}

	branch := Branch{
		Name:    rawBranch.Name,
		Project: project.PathWithNamespace,
	}

	return branch, nil
}

// func (c *DefaultClient) CreateMergeRequest(projectID interface{}, title string, description string, sourceBranch string, targetBranch string, remove bool) (MergeRequest, error) {
func (c *DefaultClient) CreateMergeRequest(params CreateMergeRequestParams) (MergeRequest, error) {
	project, err := c.findProject(params.ProjectID)
	if err != nil {
		return MergeRequest{}, err
	}

	options := &gitlab.CreateMergeRequestOptions{
		Title:              &params.Title,
		Description:        &params.Description,
		SourceBranch:       &params.SourceBranch,
		TargetBranch:       &params.TargetBranch,
		RemoveSourceBranch: &params.Remove,
	}

	mr, _, err := c.client.MergeRequests.CreateMergeRequest(project.ID, options)
	if err != nil {
		return MergeRequest{}, fmt.Errorf("error while creating Merge Request %q: %v", params.Title, err)
	}

	return mergeRequestFromGitLabMergeRequest(mr), nil
}

func (c *DefaultClient) ListMergeRequests(params ListMergeRequestsParams) (MergeRequests, error) {
	project, err := c.findProject(params.ProjectID)
	if err != nil {
		return MergeRequests{}, err
	}

	labels := gitlab.Labels(params.Label)
	options := &gitlab.ListProjectMergeRequestsOptions{
		Milestone: &params.Milestone,
		Labels:    &labels,
		State:     &params.State,
	}

	mrs, _, err := c.client.MergeRequests.ListProjectMergeRequests(project.ID, options)
	if err != nil {
		return MergeRequests{}, fmt.Errorf("error while listing Merge Requests: %v", err)
	}

	mergeRequests := make(MergeRequests, 0)
	for _, mr := range mrs {
		mergeRequests = append(mergeRequests, mergeRequestFromGitLabMergeRequest(mr))
	}

	return mergeRequests, nil
}

func mergeRequestFromGitLabMergeRequest(mr *gitlab.MergeRequest) MergeRequest {
	return MergeRequest{
		ID:          mr.ID,
		IID:         mr.IID,
		Title:       mr.Title,
		Description: mr.Description,
		WebURL:      mr.WebURL,
	}
}

func (c *DefaultClient) CreateIssue(params CreateIssueParams) (Issue, error) {
	project, err := c.findProject(params.ProjectID)
	if err != nil {
		return Issue{}, err
	}

	options := &gitlab.CreateIssueOptions{
		Title:       &params.Title,
		Description: &params.Description,
	}

	rawIssue, _, err := c.client.Issues.CreateIssue(project.ID, options)
	if err != nil {
		return Issue{}, fmt.Errorf("error while creating issue %q: %v", params.Title, err)
	}

	return issueFromGitLabIssue(rawIssue), nil
}

func (c *DefaultClient) UpdateIssue(params UpdateIssueParams) (Issue, error) {
	project, err := c.findProject(params.ProjectID)
	if err != nil {
		return Issue{}, err
	}

	options := &gitlab.UpdateIssueOptions{
		Title:       &params.Title,
		Description: &params.Description,
	}

	rawIssue, _, err := c.client.Issues.UpdateIssue(project.ID, params.IID, options)
	if err != nil {
		return Issue{}, fmt.Errorf("error while creating issue %q: %v", params.Title, err)
	}

	return issueFromGitLabIssue(rawIssue), nil
}

func (c *DefaultClient) ReadIssue(params ReadIssueParams) (Issue, error) {
	project, err := c.findProject(params.ProjectID)
	if err != nil {
		return Issue{}, err
	}

	issue, _, err := c.client.Issues.GetIssue(project.ID, params.IID)
	if err != nil {
		return Issue{}, fmt.Errorf("error while creating issue %s#%d: %v", params.ProjectID, params.IID, err)
	}

	return issueFromGitLabIssue(issue), nil
}

func issueFromGitLabIssue(issue *gitlab.Issue) Issue {
	return Issue{
		ID:          issue.ID,
		IID:         issue.IID,
		Title:       issue.Title,
		Description: issue.Description,
		WebURL:      issue.WebURL,
	}
}

func (c *DefaultClient) ReadRemoteFile(params ReadRemoteFileParams) ([]byte, error) {
	project, err := c.findProject(params.ProjectID)
	if err != nil {
		return []byte{}, err
	}

	options := &gitlab.GetRawFileOptions{
		Ref: &params.Ref,
	}

	content, _, err := c.client.RepositoryFiles.GetRawFile(project.ID, params.Path, options)
	if err != nil {
		return []byte{}, fmt.Errorf("error while reading raw file %q from %q: %v", params.Path, params.ProjectID, err)
	}

	return content, nil
}

func (c *DefaultClient) CreateLabel(params CreateLabelParams) (Label, error) {
	project, err := c.findProject(params.ProjectID)
	if err != nil {
		return Label{}, err
	}

	options := &gitlab.CreateLabelOptions{
		Name:        &params.Name,
		Description: &params.Description,
		Color:       &params.Color,
	}

	label, _, err := c.client.Labels.CreateLabel(project.ID, options)
	if err != nil {
		return Label{}, fmt.Errorf("error while creating label %q: %v", params.Name, err)
	}

	return labelFromGitLabLabel(label), nil
}

func labelFromGitLabLabel(label *gitlab.Label) Label {
	return Label{
		Name: label.Name,
	}
}

func (c *DefaultClient) findProject(projectID interface{}) (*gitlab.Project, error) {
	project, _, err := c.client.Projects.GetProject(projectID, &gitlab.GetProjectOptions{})
	if err != nil {
		return nil, fmt.Errorf("error while requesting project %q: %v", projectID, err)
	}

	return project, nil
}
