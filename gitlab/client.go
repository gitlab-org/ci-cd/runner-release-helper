package gitlab

import (
	"errors"
)

type Branch struct {
	Name    string
	Project string
}

type MergeRequest struct {
	ID          int
	IID         int
	Title       string
	WebURL      string
	Description string
}

type MergeRequests []MergeRequest

type Issue struct {
	ID          int
	IID         int
	Title       string
	Description string
	WebURL      string
}

type Label struct {
	Name string
}

type CreateBranchParams struct {
	ProjectID interface{}
	Name      string
	Ref       string
}

type CreateMergeRequestParams struct {
	ProjectID    interface{}
	Title        string
	Description  string
	SourceBranch string
	TargetBranch string
	Remove       bool
}

type ListMergeRequestsParams struct {
	ProjectID interface{}
	Milestone string
	Label     []string
	State     string
}

type CreateIssueParams struct {
	ProjectID   interface{}
	Title       string
	Description string
}

type UpdateIssueParams struct {
	ProjectID   interface{}
	IID         int
	Title       string
	Description string
}

type ReadIssueParams struct {
	ProjectID interface{}
	IID       int
}

type ReadRemoteFileParams struct {
	ProjectID interface{}
	Path      string
	Ref       string
}

type CreateLabelParams struct {
	ProjectID   interface{}
	Name        string
	Description string
	Color       string
}

type Client interface {
	CreateBranch(params CreateBranchParams) (Branch, error)
	CreateMergeRequest(params CreateMergeRequestParams) (MergeRequest, error)
	ListMergeRequests(params ListMergeRequestsParams) (MergeRequests, error)
	CreateIssue(params CreateIssueParams) (Issue, error)
	UpdateIssue(params UpdateIssueParams) (Issue, error)
	ReadIssue(params ReadIssueParams) (Issue, error)
	ReadRemoteFile(params ReadRemoteFileParams) ([]byte, error)
	CreateLabel(params CreateLabelParams) (Label, error)
}

var client Client

func ClientInitialize(baseURL string, token string, dryRun bool) error {
	if dryRun {
		client = new(DryRunClient)

		return nil
	}

	defaultClient := new(DefaultClient)
	err := defaultClient.init(baseURL, token)
	if err != nil {
		return err
	}

	client = defaultClient

	return nil
}

func ClientInstance() (Client, error) {
	if client == nil {
		return nil, errors.New("GitLab Client is uninitialized")
	}

	return client, nil
}
