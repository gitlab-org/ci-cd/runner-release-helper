package template

import (
	"fmt"

	"gitlab.com/gitlab-org/ci-cd/runner-release-helper/templates"
)

type bindataLoader struct{}

func (l *bindataLoader) Load(templateType Type, path string) ([]byte, error) {
	templatePath := fmt.Sprintf("templates/%s/%s", templateType, path)

	data, err := templates.Asset(templatePath)
	if err != nil {
		return nil, fmt.Errorf("error while reading the template from bindata %q: %v", templatePath, err)
	}

	return data, nil
}

func newBindataLoader() loader {
	return new(bindataLoader)
}
