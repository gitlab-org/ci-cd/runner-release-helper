package template

import (
	"bytes"
	"fmt"
	tpl "text/template"
)

type Template struct {
	name    string
	path    string
	ownType Type

	loader loader

	tpl *tpl.Template
}

func (t *Template) Execute(data interface{}) (string, error) {
	var output []byte
	buffer := bytes.NewBuffer(output)

	err := t.tpl.Execute(buffer, data)
	if err != nil {
		return "", fmt.Errorf("error while executing template %q: %v", t.tpl.Name(), err)
	}

	return buffer.String(), nil
}

func (t *Template) LoadPart(name string) (*Template, error) {
	path := fmt.Sprintf("%s-parts/%s", t.path, name)

	data, err := t.loader.Load(t.ownType, path)
	if err != nil {
		return nil, err
	}

	return newTemplateWithConstructor(name, path, t.ownType, data, t.loader, t.tpl.New)
}

func newTemplate(name string, templateType Type, data []byte, loader loader) (*Template, error) {
	return newTemplateWithConstructor(name, name, templateType, data, loader, tpl.New)
}

func newTemplateWithConstructor(name string, path string, templateType Type, data []byte, loader loader, constructor func(name string) *tpl.Template) (*Template, error) {
	tplTemplate := constructor(name)
	tplTemplate.Funcs(getFuncMap())

	tplTemplate, err := tplTemplate.Parse(string(data))
	if err != nil {
		return nil, fmt.Errorf("error while parsing template %q: %v", name, err)
	}

	template := &Template{
		name:    name,
		path:    path,
		ownType: templateType,
		loader:  loader,
		tpl:     tplTemplate,
	}

	return template, nil
}

func getFuncMap() tpl.FuncMap {
	return tpl.FuncMap{
		"inc": func(i int) int {
			return i + 1
		},
		"dec": func(i int) int {
			return i - 1
		},
		"tenary": func(condition bool, ifTrue interface{}, ifFalse interface{}) interface{} {
			if condition {
				return ifTrue
			}

			return ifFalse
		},
	}
}

func Load(templateType Type, nameOrPath string) (*Template, error) {
	loader, err := getLoader()
	if err != nil {
		return nil, err
	}

	data, err := loader.Load(templateType, nameOrPath)
	if err != nil {
		return nil, err
	}

	return newTemplate(nameOrPath, templateType, data, loader)
}
