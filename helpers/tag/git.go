package tag

import (
	"fmt"
	"sort"

	"github.com/hashicorp/go-version"
	"github.com/sirupsen/logrus"
)

// GitTag represent a specific Git tag.
type GitTag struct {
	// String representation of the stable branch to be used inside of
	// templates.
	StableBranch string
	Major        int
	Minor        int
	Patch        int
}

// String returns a string representation of the git tag.
func (t GitTag) String() string {
	return fmt.Sprintf("v%d.%d.%d", t.Major, t.Minor, t.Patch)
}

// GitTags is a type that implements the sort.Interface interface so that GitTag can be sorted.
type GitTags []GitTag

func (g GitTags) Len() int {
	return len(g)
}

func (g GitTags) Less(i, j int) bool {
	verI, err := version.NewVersion(g[i].String())
	if err != nil {
		logrus.WithError(err).Fatalf("Failed to generate version from tag %s", g[i].String())
	}

	verJ, err := version.NewVersion(g[j].String())
	if err != nil {
		logrus.WithError(err).Fatalf("Failed to generate version from tag %s", g[j].String())
	}

	return verI.LessThan(verJ)
}

func (g GitTags) Swap(i, j int) {
	g[i], g[j] = g[j], g[i]
}

// Latest returns the latest GitTag from the list.
func (g GitTags) Latest() GitTag {
	sort.Sort(sort.Reverse(g))

	return g[0]
}
