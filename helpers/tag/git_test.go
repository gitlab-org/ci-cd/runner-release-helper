package tag

import (
	"fmt"
	"sort"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGitTag_String(t *testing.T) {
	tag := GitTag{
		StableBranch: "13-2-stable",
		Major:        13,
		Minor:        2,
		Patch:        1,
	}

	assert.Equal(t, "v13.2.1", tag.String())
}

func TestGitTag_Sort(t *testing.T) {
	tests := []struct {
		tags         GitTags
		expectedTags GitTags
	}{
		{

			tags: GitTags{
				{
					StableBranch: "13-2-stable",
					Major:        13,
					Minor:        2,
					Patch:        1,
				},
				{
					StableBranch: "13-0-stable",
					Major:        13,
					Minor:        0,
					Patch:        1,
				},
				{
					StableBranch: "13-1-stable",
					Major:        13,
					Minor:        1,
					Patch:        2,
				},
			},
			expectedTags: GitTags{
				{
					StableBranch: "13-0-stable",
					Major:        13,
					Minor:        0,
					Patch:        1,
				},
				{
					StableBranch: "13-1-stable",
					Major:        13,
					Minor:        1,
					Patch:        2,
				},
				{
					StableBranch: "13-2-stable",
					Major:        13,
					Minor:        2,
					Patch:        1,
				},
			},
		},
		{
			tags: GitTags{
				{
					StableBranch: "13-2-stable",
					Major:        13,
					Minor:        2,
					Patch:        0,
				},
				{
					StableBranch: "13-2-stable",
					Major:        13,
					Minor:        2,
					Patch:        2,
				},
				{
					StableBranch: "13-2-stable",
					Major:        13,
					Minor:        2,
					Patch:        1,
				},
			},
			expectedTags: GitTags{
				{
					StableBranch: "13-2-stable",
					Major:        13,
					Minor:        2,
					Patch:        0,
				},
				{
					StableBranch: "13-2-stable",
					Major:        13,
					Minor:        2,
					Patch:        1,
				},
				{
					StableBranch: "13-2-stable",
					Major:        13,
					Minor:        2,
					Patch:        2,
				},
			},
		},
		{
			tags: GitTags{
				{
					StableBranch: "13-2-stable",
					Major:        13,
					Minor:        2,
				},
				{
					StableBranch: "13-2-stable",
					Major:        13,
					Minor:        2,
				},
				{
					StableBranch: "13-2-stable",
					Major:        13,
					Minor:        2,
				},
			},
			expectedTags: GitTags{
				{
					StableBranch: "13-2-stable",
					Major:        13,
					Minor:        2,
					Patch:        0,
				},
				{
					StableBranch: "13-2-stable",
					Major:        13,
					Minor:        2,
					Patch:        0,
				},
				{
					StableBranch: "13-2-stable",
					Major:        13,
					Minor:        2,
					Patch:        0,
				},
			},
		},
	}

	for i, tt := range tests {
		t.Run(fmt.Sprintf("%d", i), func(t *testing.T) {
			sort.Sort(tt.tags)
			assert.Equal(t, tt.expectedTags, tt.tags)
		})
	}
}

func TestGitTags_Latest(t *testing.T) {
	tests := []struct {
		tags        GitTags
		expectedTag GitTag
	}{
		{

			tags: GitTags{
				{
					StableBranch: "13-2-stable",
					Major:        13,
					Minor:        2,
					Patch:        1,
				},
				{
					StableBranch: "13-0-stable",
					Major:        13,
					Minor:        0,
					Patch:        1,
				},
				{
					StableBranch: "13-1-stable",
					Major:        13,
					Minor:        1,
					Patch:        2,
				},
			},
			expectedTag: GitTag{
				StableBranch: "13-2-stable",
				Major:        13,
				Minor:        2,
				Patch:        1,
			},
		},
		{
			tags: GitTags{
				{
					StableBranch: "13-2-stable",
					Major:        13,
					Minor:        2,
					Patch:        0,
				},
				{
					StableBranch: "13-2-stable",
					Major:        13,
					Minor:        2,
					Patch:        2,
				},
				{
					StableBranch: "13-2-stable",
					Major:        13,
					Minor:        2,
					Patch:        1,
				},
			},
			expectedTag: GitTag{
				StableBranch: "13-2-stable",
				Major:        13,
				Minor:        2,
				Patch:        2,
			},
		},
		{
			tags: GitTags{
				{
					StableBranch: "13-2-stable",
					Major:        13,
					Minor:        2,
				},
				{
					StableBranch: "13-2-stable",
					Major:        13,
					Minor:        2,
				},
				{
					StableBranch: "13-2-stable",
					Major:        13,
					Minor:        2,
				},
			},
			expectedTag: GitTag{
				StableBranch: "13-2-stable",
				Major:        13,
				Minor:        2,
			},
		},
	}

	for i, tt := range tests {
		t.Run(fmt.Sprintf("%d", i), func(t *testing.T) {
			latest := tt.tags.Latest()
			assert.Equal(t, tt.expectedTag, latest)
		})
	}
}
