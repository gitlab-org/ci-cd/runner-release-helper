package tag

import (
	"sort"

	"github.com/hashicorp/go-version"
	"github.com/sirupsen/logrus"
)

// ReleaseTag represents a collection of GitTag that are considered to be the
// same release, just different repositories.
type ReleaseTag struct {
	Runner    GitTag
	HelmChart GitTag
}

// ReleaseTags is a type that implements the sort.Interface interface so that
// ReleaseTags can be sorted. It will use the Runner tag for sorting and not the
// helm chart.
type ReleaseTags []ReleaseTag

func (r ReleaseTags) Len() int {
	return len(r)
}

func (r ReleaseTags) Less(i, j int) bool {
	verI, err := version.NewVersion(r[i].Runner.String())
	if err != nil {
		logrus.WithError(err).Fatalf("Failed to generate version from tag %s", r[i].Runner.String())
	}

	verJ, err := version.NewVersion(r[j].Runner.String())
	if err != nil {
		logrus.WithError(err).Fatalf("Failed to generate version from tag %s", r[j].Runner.String())
	}

	return verI.LessThan(verJ)
}

func (r ReleaseTags) Swap(i, j int) {
	r[i], r[j] = r[j], r[i]
}

// Latest returns the latest ReleaseTag from the list.
func (r ReleaseTags) Latest() ReleaseTag {
	sort.Sort(sort.Reverse(r))

	return r[0]
}
