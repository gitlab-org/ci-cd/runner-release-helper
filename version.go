package rrhelper

import (
	"fmt"
	"runtime"
)

var (
	NAME     = "rrhelper"
	VERSION  = "dev"
	REVISION = "HEAD"
	BRANCH   = "HEAD"
	BUILT    = "unknown"
)

type Version struct {
	Name         string
	Version      string
	Revision     string
	Branch       string
	GOVersion    string
	BuiltAt      string
	OS           string
	Architecture string
}

func (v *Version) UserAgent() string {
	return fmt.Sprintf("%s %s (%s; %s; %s/%s)", v.Name, v.Version, v.Branch, v.GOVersion, v.OS, v.Architecture)
}

func (v *Version) ShortLine() string {
	return fmt.Sprintf("%s (%s)", v.Version, v.Revision)
}

func (v *Version) Extended() string {
	version := fmt.Sprintln(v.Name)
	version += fmt.Sprintf("Version:      %s\n", v.Version)
	version += fmt.Sprintf("Git revision: %s\n", v.Revision)
	version += fmt.Sprintf("Git branch:   %s\n", v.Branch)
	version += fmt.Sprintf("GO version:   %s\n", v.GOVersion)
	version += fmt.Sprintf("Built:        %s\n", v.BuiltAt)
	version += fmt.Sprintf("OS/Arch:      %s/%s\n", v.OS, v.Architecture)

	return version
}

var version *Version

func VersionInstance() *Version {
	if version != nil {
		return version
	}

	version = &Version{
		Name:         NAME,
		Version:      VERSION,
		Revision:     REVISION,
		Branch:       BRANCH,
		GOVersion:    runtime.Version(),
		BuiltAt:      BUILT,
		OS:           runtime.GOOS,
		Architecture: runtime.GOARCH,
	}

	return version
}
