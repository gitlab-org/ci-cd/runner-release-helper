package main

import (
	"gitlab.com/gitlab-org/ci-cd/runner-release-helper/app"
	"gitlab.com/gitlab-org/ci-cd/runner-release-helper/commands"
)

func main() {
	defer app.Recover()

	application := app.New()
	application.AddCommands(app.CommandDefinitions{
		{
			Name:   "create-release-checklist",
			Usage:  "Creates the Release Checklist issue",
			Action: commands.NewCreateReleaseChecklist(),
		},
		{
			Name:   "create-next-rc-checklist",
			Usage:  "Updates the Release Checklist issue with steps for the new RC",
			Action: commands.NewCreateNextRCChecklist(),
		},
		{
			Name:   "create-branch",
			Usage:  "Creates a branch in specified project",
			Action: commands.NewCreateBranch(),
		},
		{
			Name:   "create-merge-request",
			Usage:  "Creates a Merge Request in specified project, using the specified template",
			Action: commands.NewCreateMergeRequest(),
		},
		{
			Name:   "list-pick-mrs-for-rc",
			Usage:  "List the 'Pick into X.Y' MRs for the specified RC",
			Action: commands.NewListPickMrsForRC(),
		},
		{
			Name:   "create-label",
			Usage:  "Creates a Label in specified project",
			Action: commands.NewCreateLabel(),
		},
		{
			Name:   "create-security-release-checklist",
			Usage:  "Creates the main Security issue",
			Action: commands.NewCreateSecurityReleaseChecklist(),
		},
	})

	application.Run()
}
