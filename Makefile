export VERSION ?= $(shell (scripts/version 2>/dev/null || echo "dev") | sed -e 's/^v//g')
export REVISION ?= $(shell git rev-parse --short HEAD || echo "unknown")
export BRANCH ?= $(shell git show-ref | grep "$(REVISION)" | grep -v HEAD | awk '{print $$2}' | sed 's|refs/remotes/origin/||' | sed 's|refs/heads/||' | sort | head -n 1)
export BUILT ?= $(shell date -u +%Y-%m-%dT%H:%M:%S%z)
export CGO_ENABLED ?= 0
export GOPATH ?= ".go/"
export BUILD_PLATFORMS ?= -osarch 'linux/amd64' -osarch 'darwin/amd64'

export goJunitReport ?= $(GOPATH)/bin/go-junit-report
gox ?= $(GOPATH)/bin/gox
goBindata ?= $(GOPATH)/bin/go-bindata

MOCKERY_VERSION = 1.1.0
mockery ?= .tmp/mockery-$(MOCKERY_VERSION)
RELEASE_INDEX_GEN_VERSION ?= master
releaseIndexGen ?= .tmp/release-index-gen-$(RELEASE_INDEX_GEN_VERSION)
GITLAB_CHANGELOG_VERSION ?= master
gitlabChangelog ?= .tmp/gitlab-changelog-$(GITLAB_CHANGELOG_VERSION)

PKG := $(shell go list .)
PKGs := $(shell go list ./... | grep -vE "^/vendor/")

GO_LDFLAGS := -X $(PKG).VERSION=$(VERSION) \
              -X $(PKG).REVISION=$(REVISION) \
              -X $(PKG).BRANCH=$(BRANCH) \
              -X $(PKG).BUILT=$(BUILT) \
              -X $(PKG)/helpers/template.usedLoaderType=bindata \
              -s -w

.PHONY: compile
compile: templates/bindata.go
	go build \
			-o build/rrhelper \
			-ldflags "$(GO_LDFLAGS)" \
			./cmd/rrhelper

.PHONY: compile_all
compile_all: $(gox)
compile_all: templates/bindata.go
	# Building project in version $(VERSION) for $(BUILD_PLATFORMS)
	$(gox) $(BUILD_PLATFORMS) \
			-ldflags "$(GO_LDFLAGS)" \
			-output="build/rrhelper-{{.OS}}-{{.Arch}}" \
			./cmd/rrhelper

templateFiles := $(shell find templates/ -type f ! -name "bindata.go")

templates/bindata.go: $(templateFiles)
templates/bindata.go: $(goBindata)
	@$(goBindata) -ignore bindata.go -o templates/bindata.go -pkg templates ./templates/...
	@go fmt ./templates/bindata.go

export testsDir = ./.tests

.PHONY: tests
tests: $(testsDir) $(goJunitReport)
	@./scripts/tests normal

.PHONY: tests_race
tests_race: $(testsDir) $(goJunitReport)
	@./scripts/tests race

.PHONY: lint
lint: OUT_FORMAT ?= colored-line-number
lint: LINT_FLAGS ?=
lint:
	@golangci-lint run ./... --out-format $(OUT_FORMAT) $(LINT_FLAGS)

$(testsDir):
	# Preparing tests output directory
	@mkdir -p $@

.PHONY: fmt
fmt:
	# Fixing project code formatting...
	@go fmt $(PKGs) | awk '{if (NF > 0) {if (NR == 1) print "Please run go fmt for:"; print "- "$$1}} END {if (NF > 0) {if (NR > 0) exit 1}}'

.PHONY: mocks
mocks: $(mockery)
	# Removing existing mocks
	@find * -name "mock_*.go" -delete
	# Generating new mocks
	@$(mockery) -recursive -all -inpkg -dir ./

.PHONY: check_mocks
check_mocks:
	# Checking if mocks are up-to-date
	# Saving the list of committed mocks
	@git status -sb > /tmp/mocks-$${CI_JOB_ID}-before
	@$(MAKE) mocks
	# Saving the list of created mocks
	@git status -sb > /tmp/mocks-$${CI_JOB_ID}-after
	# Checking the difference
	@diff -U0 /tmp/mocks-$${CI_JOB_ID}-before /tmp/mocks-$${CI_JOB_ID}-after

.PHONY: check_modules
check_modules:
	@git diff go.sum > /tmp/gosum-$${CI_JOB_ID}-before
	@go mod tidy
	@git diff go.sum > /tmp/gosum-$${CI_JOB_ID}-after
	@diff -U0 /tmp/gosum-$${CI_JOB_ID}-before /tmp/gosum-$${CI_JOB_ID}-after

.PHONY: prepare_ci_image
prepare_ci_image: CI_IMAGE ?= gitlab-changelog
prepare_ci_image: CI_REGISTRY ?= ""
prepare_ci_image:
	# Builiding the $(CI_IMAGE) image
	@docker build \
			--pull \
			--no-cache \
			--build-arg GO_VERSION=$${GO_VERSION} \
			--build-arg ALPINE_VERSION=$${ALPINE_VERSION} \
			--build-arg GCLOUD_SDK_VERSION=$${GCLOUD_SDK_VERSION} \
			-t $(CI_IMAGE) \
			-f dockerfiles/ci/Dockerfile \
			dockerfiles/ci/
ifneq ($(CI_REGISTRY),)
	# Pushing the $(CI_IMAGE) image to $(CI_REGISTRY)
	@docker login --username $${CI_REGISTRY_USER} --password $${CI_REGISTRY_PASSWORD} $(CI_REGISTRY)
	@docker push $(CI_IMAGE)
	@docker logout $(CI_REGISTRY)
else
	# No CI_REGISTRY value, skipping image push
endif

.PHONY: release_gcs
release_gcs: CI_COMMIT_REF_NAME ?= $(BRANCH)
release_gcs: CI_COMMIT_SHA ?= $(REVISION)
release_gcs: GCS_BUCKET ?=
release_gcs: GCS_PATH ?=
release_gcs: latest_stable_tag := $(shell git -c versionsort.prereleaseSuffix="-rc" tag -l "v*.*.*" --sort=-v:refname | awk '!/rc/' | head -n 1)
release_gcs:
	# Preparing index page
	@$(MAKE) index_file
ifneq ($(GCS_BUCKET),)
	@$(MAKE) sync_gcs_release GCS_URL="gs://$(GCS_BUCKET)/$(GCS_PATH)/$(CI_COMMIT_REF_NAME)/"
	@if [ $$(git describe --exact-match --match $(latest_stable_tag) >/dev/null 2>&1) ]; then \
			@$(MAKE) sync_gcs_release GCS_URL="gs://$(GCS_BUCKET)/$(GCS_PATH)/latest/"; \
	fi
	@$(MAKE) release_gitlab
endif

.PHONY: sync_gcs_release
sync_gcs_release: GCS_URL ?=
sync_gcs_release:
	# Syncing with $(GCS_URL)
	@gsutil rsync build/ "$(GCS_URL)"
	@gsutil acl set -r public-read "$(GCS_URL)"

.PHONY: remove_gcs_release
remove_gcs_release: CI_COMMIT_REF_NAME ?= $(BRANCH)
remove_gcs_release: GCS_BUCKET ?=
remove_gcs_release: GCS_PATH ?=
remove_gcs_release:
ifneq ($(GCS_BUCKET),)
	@gsutil rm -r "gs://$(GCS_BUCKET)/$(GCS_PATH)/$(CI_COMMIT_REF_NAME)"
endif

.PHONY: release_gitlab
release_gitlab: export CI_COMMIT_TAG ?=
release_gitlab: export CI_PROJECT_URL ?=
release_gitlab:
ifneq ($(CI_COMMIT_TAG),)
	# Saving as GitLab release at $(CI_PROJECT_URL)/-/releases
	@./scripts/gitlab_release
endif

.PHONY: index_file
index_file: export CI_COMMIT_REF_NAME ?= $(BRANCH)
index_file: export CI_COMMIT_SHA ?= $(REVISION)
index_file: $(releaseIndexGen)
	# generating index.html file
	@$(releaseIndexGen) \
		-working-directory build/ \
		-project-version $(VERSION) \
		-project-git-ref $(CI_COMMIT_REF_NAME) \
		-project-git-revision $(CI_COMMIT_SHA) \
		-project-name "GitLab Runner Release Helper" \
		-project-repo-url "https://gitlab.com/gitlab-org/ci-cd/runner-release-helper" \
		-gpg-key-env GPG_KEY \
		-gpg-password-env GPG_PASSPHRASE

.PHONY: generate_changelog
generate_changelog: export CHANGELOG_RELEASE ?= $(VERSION)
generate_changelog: $(gitlabChangelog)
	@$(gitlabChangelog) \
		-changelog-file CHANGELOG.md \
		-config-file .gitlab/changelog.yml \
		-project-id 11173751 \
		-release $(CHANGELOG_RELEASE) \
		-starting-point-matcher "v[0-9]*.[0-9]*.[0-9]*"

build/gitlab-changelog:
	@$(MAKE) compile

$(mockery): OS_TYPE ?= $(shell uname -s)
$(mockery): DOWNLOAD_URL = "https://github.com/vektra/mockery/releases/download/v$(MOCKERY_VERSION)/mockery_$(MOCKERY_VERSION)_$(OS_TYPE)_x86_64.tar.gz"
$(mockery):
	# Installing $(DOWNLOAD_URL) as $(mockery)
	@mkdir -p $(shell dirname $(mockery))
	@curl -sL "$(DOWNLOAD_URL)" | tar xz -O mockery > $(mockery)
	@chmod +x "$(mockery)"

$(goJunitReport):
	# Installing $(goJunitReport)
	@go get github.com/jstemmer/go-junit-report

$(gox):
	# Installing $(gox)
	@go get github.com/mitchellh/gox

$(goBindata):
	# Installing $(goBindata)
	@go get github.com/shuLhan/go-bindata/cmd/go-bindata

$(releaseIndexGen): OS_TYPE ?= $(shell uname -s | tr '[:upper:]' '[:lower:]')
$(releaseIndexGen): DOWNLOAD_URL = "https://storage.googleapis.com/gitlab-runner-tools/release-index-generator/$(RELEASE_INDEX_GEN_VERSION)/release-index-gen-$(OS_TYPE)-amd64"
$(releaseIndexGen):
	# Installing $(DOWNLOAD_URL) as $(releaseIndexGen)
	@mkdir -p $(shell dirname $(releaseIndexGen))
	@curl -sL "$(DOWNLOAD_URL)" -o "$(releaseIndexGen)"
	@chmod +x "$(releaseIndexGen)"

$(gitlabChangelog): OS_TYPE ?= $(shell uname -s | tr '[:upper:]' '[:lower:]')
$(gitlabChangelog): DOWNLOAD_URL = "https://storage.googleapis.com/gitlab-runner-tools/gitlab-changelog/$(GITLAB_CHANGELOG_VERSION)/gitlab-changelog-$(OS_TYPE)-amd64"
$(gitlabChangelog):
	# Installing $(DOWNLOAD_URL) as $(gitlabChangelog)
	@mkdir -p $(shell dirname $(gitlabChangelog))
	@curl -sL "$(DOWNLOAD_URL)" -o "$(gitlabChangelog)"
	@chmod +x "$(gitlabChangelog)"
