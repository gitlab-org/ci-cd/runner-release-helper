## At 20th - the release day of GitLab Runner

- [ ] check if Pipeline for `{{.Major}}-{{.Minor}}-stable` is passing: [![pipeline status](https://gitlab.com/gitlab-org/gitlab-runner/badges/{{.Major}}-{{.Minor}}-stable/pipeline.svg)](https://gitlab.com/gitlab-org/gitlab-runner/commits/{{.Major}}-{{.Minor}}-stable)
    - [ ] add all required fixes to make `{{.Major}}-{{.Minor}}-stable` Pipeline passing
- [ ] `git checkout {{.Major}}-{{.Minor}}-stable && git pull` in your local working copy!
- [ ] merge all RCx CHANGELOG entries into release entry

    Also add all missing entries (should not happen at this moment unless an RC release was
    consciously skipped) and put a proper header at the beginning:

    ```bash
    make generate_changelog CHANGELOG_RELEASE=v{{.Major}}.{{.Minor}}.0
    ```

    - [ ] merge all RC entries

- [ ] add **v{{.Major}}.{{.Minor}}.0** CHANGELOG entries and commit

    ```bash
    git add CHANGELOG.md && \
    git commit -m "Update CHANGELOG for v{{.Major}}.{{.Minor}}.0" -S
    ```

- [ ] tag and push **v{{.Major}}.{{.Minor}}.0** and **{{.Major}}-{{.Minor}}-stable**:

    ```bash
    git tag -s v{{.Major}}.{{.Minor}}.0 -m "Version v{{.Major}}.{{.Minor}}.0" && \
    git push origin v{{.Major}}.{{.Minor}}.0 {{.Major}}-{{.Minor}}-stable
    ```

- [ ] checkout to `main` and merge `{{.Major}}-{{.Minor}}-stable` into `main`, push `main`:

    ```bash
    git checkout main && \
    git pull && \
    git merge --no-ff {{.Major}}-{{.Minor}}-stable
    # check that the only changes are in CHANGELOG.md
    git push
    ```

- [ ] update runner [helm chart](https://gitlab.com/gitlab-org/charts/gitlab-runner) to use `v{{.Major}}.{{.Minor}}.0` version
    - [ ] check if Pipeline for `{{.HelmChartMajor}}-{{.HelmChartMinor}}-0-stable` is passing: [![pipeline status](https://gitlab.com/gitlab-org/charts/gitlab-runner/badges/{{.HelmChartMajor}}-{{.HelmChartMinor}}-0-stable/pipeline.svg)](https://gitlab.com/gitlab-org/charts/gitlab-runner/commits/{{.HelmChartMajor}}-{{.HelmChartMinor}}-0-stable)
        - [ ] add all required fixes to make `{{.HelmChartMajor}}-{{.HelmChartMinor}}-0-stable` Pipeline passing
    - [ ] go to your local working copy of https://gitlab.com/gitlab-org/charts/gitlab-runner
    - [ ] `git checkout {{.HelmChartMajor}}-{{.HelmChartMinor}}-0-stable && git pull` in your local working copy!
    - [ ] update GitLab Runner version

        ```bash
        sed -i".bak" "s/^appVersion: .*/appVersion: {{.Major}}.{{.Minor}}.0/" Chart.yaml && \
        rm Chart.yaml.bak && \
        git add Chart.yaml && \
        git commit -m "Update used GitLab Runner version to {{.Major}}.{{.Minor}}.0" -S
        ```

    - [ ] bump version of the Helm Chart to `{{.HelmChartMajor}}.{{.HelmChartMinor}}.0`

        ```bash
        sed -i".bak" "s/^version: .*/version: {{.HelmChartMajor}}.{{.HelmChartMinor}}.0/" Chart.yaml && \
        rm Chart.yaml.bak && \
        git add Chart.yaml && \
        git commit -m "Bump version to {{.HelmChartMajor}}.{{.HelmChartMinor}}.0" -S
        ```

    - [ ] merge all RCx CHANGELOG entries into release entry

        Also add all missing entries (should not happen at this moment unless an RC release was
        consciously skipped) and put a proper header at the beginning:

        ```bash
        make generate_changelog CHANGELOG_RELEASE=v{{.HelmChartMajor}}.{{.HelmChartMinor}}.0
        ```

        - [ ] merge all RC entries
        - [ ] remove all `Update GitLab Runner version to {{.Major}}.{{.Minor}}.0-rcX` lines from the changes list
        - [ ] manually add line including GitLab Runner version update information as the first item under `New features`

            ```markdown
            - Update GitLab Runner version to {{.Major}}.{{.Minor}}.0
            ```

    - [ ] add **v{{.HelmChartMajor}}.{{.HelmChartMinor}}.0** CHANGELOG entries and commit

        ```bash
        git add CHANGELOG.md && \
        git commit -m "Update CHANGELOG for v{{.HelmChartMajor}}.{{.HelmChartMinor}}.0" -S
        ```

    - [ ] tag and push **v{{.HelmChartMajor}}.{{.HelmChartMinor}}.0** and **{{.HelmChartMajor}}-{{.HelmChartMinor}}-0-stable**:

        ```bash
        git tag -s v{{.HelmChartMajor}}.{{.HelmChartMinor}}.0 -m "Version v{{.HelmChartMajor}}.{{.HelmChartMinor}}.0" && \
        git push origin v{{.HelmChartMajor}}.{{.HelmChartMinor}}.0 {{.HelmChartMajor}}-{{.HelmChartMinor}}-0-stable
        ```

    - [ ] checkout to `main` and merge `{{.HelmChartMajor}}-{{.HelmChartMinor}}-0-stable` into `main` (only this one time, to update CHANGELOG.md and make the tag available for `./scripts/prepare-changelog-entries.rb` in next stable release), push `main`:

        ```bash
        git checkout main && \
        git pull && \
        git merge --no-ff {{.HelmChartMajor}}-{{.HelmChartMinor}}-0-stable
        # check that the only changes are in CHANGELOG.md
        git push
        ```

- [ ] update Runner's chart version [used by Cluster Management](https://gitlab.com/gitlab-org/project-templates/cluster-management/-/blob/master/applications/gitlab-runner/helmfile.yaml#L9): link_to_MR__ghc_rhcvu

    - [ ] create branch

        ```bash
        rrhelper \
            --dry-run \
            create-branch \
            gitlab-org/project-templates/cluster-management \
            update-gitlab-runner-helm-chart-to-{{.HelmChartMajor}}-{{.HelmChartMinor}}-0
        ```

    - [ ] create Merge Request

        ```bash
        rrhelper \
            --dry-run \
            create-merge-request \
            --release-checklist-issue {{.ReleaseChecklistIssueID}} \
            --replace-link-in-release-checklist-issue link_to_MR__ghc_rhcvu \
            --milestone {{.Major}}.{{.Minor}} \
            --runner-version {{.Major}}.{{.Minor}}.0 \
            --helm-chart-version {{.HelmChartMajor}}.{{.HelmChartMinor}}.0 \
            gitlab-org/project-templates/cluster-management \
            update-gitlab-runner-helm-chart-to-{{.HelmChartMajor}}-{{.HelmChartMinor}}-0 \
            master \
            runner-helm-chart-upgrade-in-cluster-management \
            "feat: Update GitLab Runner Helm Chart to {{.HelmChartMajor}}.{{.HelmChartMinor}}.0/{{.Major}}.{{.Minor}}.0"
        ```

    - [ ] Adjust and apply the patch to the GitLab Helm Chart sources

        <details>
        <summary>See the patch draft</summary>

        - [ ] Adjust the following patch (set proper current version; set the Merge Request ID in the CHANGELOG entry file)
        and save it at `/tmp/patch.gitlab-cluster-management` (In case the diff can't apply, do it manually).

            ```diff
            cat > /tmp/patch.gitlab-cluster-management << EOF
            diff --git a/applications/gitlab-runner/helmfile.yaml b/applications/gitlab-runner/helmfile.yaml
            index 5b97c91..7799d50 100644
            --- a/applications/gitlab-runner/helmfile.yaml
            +++ b/applications/gitlab-runner/helmfile.yaml
            @@ -6,7 +6,7 @@ releases:
             - name: runner
               namespace: gitlab-managed-apps
               chart: gitlab/gitlab-runner
            -  version: {{.HelmChartMajor}}.{{dec .HelmChartMinor}}.0
            +  version: {{.HelmChartMajor}}.{{.HelmChartMinor}}.0
               installed: true
               values:
                 - values.yaml.gotmpl
            EOF
            ```

        - [ ] Go to your local GitLab Cluster Application working directory:

            ```bash
            git checkout master && \
                git pull && \
                git checkout update-gitlab-runner-helm-chart-to-{{.HelmChartMajor}}-{{.HelmChartMinor}}-0 && \
                git apply /tmp/patch.gitlab-cluster-management && \
                git add . && \
                git commit -m "feat: Update GitLab Runner Helm Chart to {{.HelmChartMajor}}.{{.HelmChartMinor}}.0"
            ```

        - [ ] Push changes

            ```bash
            git push -u origin update-gitlab-runner-helm-chart-to-{{.HelmChartMajor}}-{{.HelmChartMinor}}-0
            ```

        - [ ] Make sure to push the Merge Request to being merged. Browse previous version bump MRs or recently merged MRs in the project in order to find a maintainer to do the review for you.

        </details>
    
- [ ] unassign yourself from this issue and assign it to the release manager responsible for the deployment


## At 22nd - the official day of GitLab release

- [ ] wait for Pipeline for `v{{.Major}}.{{.Minor}}.0` to pass [![pipeline status](https://gitlab.com/gitlab-org/gitlab-runner/badges/v{{.Major}}.{{.Minor}}.0/pipeline.svg)](https://gitlab.com/gitlab-org/gitlab-runner/commits/v{{.Major}}.{{.Minor}}.0)
    - [ ] add all required fixes to make `v{{.Major}}.{{.Minor}}.0` passing
- [ ] deploy **v{{.Major}}.{{.Minor}}.0**

    Detailed description of Runners deployment for GitLab.com CI fleet can be found
    [in the runbook](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/ci-runners/linux/deployment.md).

    - [ ] at release day: to `prmX` runners

        - [ ] make sure it's not inside of the [PCL time window](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/ci-runners/README.md#production-change-lock-pcl).
        - [ ] go to your local [`chef-repo`](https://gitlab.com/gitlab-com/gl-infra/chef-repo) working directory and execute:

            ```bash
            knife ssh -afqdn 'roles:gitlab-runner-prm' -- 'sudo -i /root/runner_upgrade.sh stop_chef'
            knife ssh -afqdn 'roles:gitlab-runner-prm' -- 'sudo -i systemctl is-active chef-client'
            git checkout master && git pull
            git checkout -b update-prm-runners-to-{{.Major}}-{{.Minor}}-0
            ```

        - [ ] update version

            ```bash
            $EDITOR roles/gitlab-runner-prm.json
            ```

            In the role definition prepare the `override_attributes` entry. It should be placed
            at the top of the file:

            ```json
            "override_attributes": {
              "cookbook-gitlab-runner": {
                "gitlab-runner": {
                    "repository": "gitlab-runner",
                    "version": "{{.Major}}.{{.Minor}}.0"
                }
              }
            },
            ```

        - [ ] `git add roles/gitlab-runner-prm.json && git commit -m "Update prmX runners to v{{.Major}}.{{.Minor}}.0"`
        - [ ] `git push -u origin update-prm-runners-to-{{.Major}}-{{.Minor}}-0 -o merge_request.create -o merge_request.label="deploy" -o merge_request.label="group::runner"`
        - [ ] inform the EOC `@sre-oncall` inside of [`#production`](https://gitlab.slack.com/archives/C101F3796) slack channel: `@sre-oncall I'll be starting the deploy for v{{.Major}}.{{.Minor}}.0 on prmX, as part of the monthly release. $LINK_TO_CHEF_REPO_MERGE_REQUEST`
        - [ ] merge the [`chef-repo` MR](https://gitlab.com/gitlab-com/gl-infra/chef-repo/merge_requests)
        - [ ] check the `production_dry_run` job if it tries to update only the changed role
        - [ ] start the manual `apply to prod` job
        - [ ] after the job is finished execute:

            ```bash
            knife ssh -C 1 -afqdn 'roles:gitlab-runner-prm' -- 'sudo -i /root/runner_upgrade.sh' &
            time wait
            ```

    - [ ] next day (if no problems): to rest of the runners

        - [ ] make sure it's not inside of the [PCL time window](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/ci-runners/README.md#production-change-lock-pcl).
        - [ ] go to your local [`chef-repo`](https://gitlab.com/gitlab-com/gl-infra/chef-repo) working directory and execute:

            ```bash
            knife ssh -afqdn 'roles:gitlab-runner-gsrm OR roles:gitlab-runner-srm OR roles:org-ci-base-runner' -- 'sudo -i /root/runner_upgrade.sh stop_chef'
            knife ssh -afqdn 'roles:gitlab-runner-gsrm OR roles:gitlab-runner-srm OR roles:org-ci-base-runner' -- 'sudo -i systemctl is-active chef-client'
            git checkout master && git pull
            git checkout -b update-runners-to-{{.Major}}-{{.Minor}}-0
            ```

        - [ ] update version for gsrm/srm

            ```bash
            $EDITOR roles/gitlab-runner-base.json
            ```

            In the role definition prepare the `gitlab-runner` entry:

            ```json
            "cookbook-gitlab-runner": {
              "gitlab-runner": {
                "repository": "gitlab-runner",
                "version": "{{.Major}}.{{.Minor}}.0"
              }
            }
            ```

        - [ ] update version for org-ci

            ```bash
            $EDITOR roles/org-ci-base-runner.json
            ```

            In the role definition prepare the `gitlab-runner` entry:

            ```json
            "cookbook-gitlab-runner": {
              "gitlab-runner": {
                "repository": "gitlab-runner",
                "version": "{{.Major}}.{{.Minor}}.0"
              }
            }
            ```

        - [ ] remove overrides from `prmX` runners

            ```bash
            $EDITOR roles/gitlab-runner-prm.json
            ```

        - [ ] `git add roles/gitlab-runner-prm.json roles/gitlab-runner-base.json roles/org-ci-base-runner.json && git commit -m "Update runners to v{{.Major}}.{{.Minor}}.0"`
        - [ ] `git push -u origin update-runners-to-{{.Major}}-{{.Minor}}-0 -o merge_request.create -o merge_request.label="deploy" -o merge_request.label="group::runner"`
        - [ ] inform the EOC `@sre-oncall` inside of [`#production`](https://gitlab.slack.com/archives/C101F3796) slack channel: `@sre-oncall I'll be starting the deploy for v{{.Major}}.{{.Minor}}.0 on runner fleet, as part of the monthly release. $LINK_TO_CHEF_REPO_MERGE_REQUEST`
        - [ ] merge the [`chef-repo` MR](https://gitlab.com/gitlab-com/gl-infra/chef-repo/merge_requests)
        - [ ] check the `production_dry_run` job if it tries to update only the changed role
        - [ ] start the manual `apply to prod` job
        - [ ] after the job is finished execute (we're not touching `prmX` - they are already updated):

            ```bash
            knife ssh -C1 -afqdn 'roles:gitlab-runner-gsrm' -- 'sudo -i /root/runner_upgrade.sh' &
            knife ssh -C1 -afqdn 'roles:gitlab-runner-srm' -- 'sudo -i /root/runner_upgrade.sh' &
            knife ssh -C1 -afqdn 'roles:org-ci-base-runner' -- 'sudo -i /root/runner_upgrade.sh' &
            time wait
            ```
