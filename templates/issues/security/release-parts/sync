### Sync

At this stage the tags, and the security commits on the stable branches are only
available in
[Security/GitLab-Runner](https://gitlab.com/gitlab-org/security/gitlab-runner),
and
[Security/Charts/GitLab-Runner](https://gitlab.com/gitlab-org/charts/security/gitlab-runner)
and we need to make these available to the canonical repositories in
[gitlab-org/gitlab-runner](https://gitlab.com/gitlab-org/gitlab-runner) and
[gitlab-org/charts/gitlab-runner](https://gitlab.com/gitlab-org/charts/gitlab-runner).

#### GitLab Runner

- [ ] Update `gitlab-org/gitlab-runner`
    - [ ] Go to a local copy of `gitlab-runner` which has both the
    [security](https://gitlab.com/gitlab-org/security/gitlab-runner), and
    [canonical](https://gitlab.com/gitlab-org/gitlab-runner) remotes

        ```shell
        origin  git@gitlab.com:gitlab-org/gitlab-runner.git (fetch)
        origin  git@gitlab.com:gitlab-org/gitlab-runner.git (push)
        security        git@gitlab.com:gitlab-org/security/gitlab-runner.git (fetch)
        security        git@gitlab.com:gitlab-org/security/gitlab-runner.git (push)
        ```
    - [ ] Run `scripts/security-harness` to turn it OFF so you can push to `origin`.
    - [ ] Make sure you have tags accessible locally `git fetch security`
    - [ ] Sync Tags
        {{- range .Tags}}
        - [ ] Push tag to origin `git push -o ci.skip origin {{.Runner.String}}`
        {{- end}}
    - [ ] Sync stables branches
        {{- range .Tags}}
        - [ ] Update `{{.Runner.StableBranch}}`
            - [ ] Check out security stable branch `git checkout {{.Runner.StableBranch}}`
            - [ ] Set the upstream to `security`: `git branch --set-upstream-to security/{{.Runner.StableBranch}} {{.Runner.StableBranch}}`
            - [ ] Make sure all your changes locally are the same as remote `git reset --hard security/{{.Runner.StableBranch}}`
            - [ ] Push changes to origin `git push -u origin {{.Runner.StableBranch}}`
        {{- end}}
    - [ ] Sync `main` branch
        - [ ] Confirm that the difference between them are the security merge commits and anything that was merged into `origin`: `git log origin/main...security/main`
        - [ ] Merge `security/main` to `main`

            ```shell
            git checkout main && \
            git pull origin main && \
            git fetch security && \
            git merge --no-ff security/main
            # Check that the correct commits are merged
            git log origin/main...
            # Push to origin
            git push -u origin main
            ```

#### Helm Chart

- [ ] Update `gitlab-org/charts/gitlab-runner`
    - [ ] Go to a local copy of `charts/gitlab-runner` which has both the
    [security](https://gitlab.com/gitlab-org/security/charts/gitlab-runner), and
    [canonical](https://gitlab.com/gitlab-org/charts/gitlab-runner) remotes

        ```shell
        origin  git@gitlab.com:gitlab-org/charts/gitlab-runner.git (fetch)
        origin  git@gitlab.com:gitlab-org/charts/gitlab-runner.git (push)
        security        git@gitlab.com:gitlab-org/security/charts/gitlab-runner.git (fetch)
        security        git@gitlab.com:gitlab-org/security/charts/gitlab-runner.git (push)
        ```
    - [ ] Run `scripts/security-harness` to turn it OFF so you can push to `origin`.
    - [ ] Make sure you have tags accessible locally `git fetch security`
    - [ ] Sync Tags
        {{- range .Tags}}
        - [ ] Push tag to origin `git push -o ci.skip origin {{.HelmChart.String}}`
        {{- end}}
    - [ ] Sync stables branches
        {{- range .Tags}}
        - [ ] Update `{{.HelmChart.StableBranch}}`
            - [ ] Check out security stable branch `git checkout {{.HelmChart.StableBranch}}`
            - [ ] Set the upstream to `security`: `git branch --set-upstream-to security/{{.HelmChart.StableBranch}} {{.HelmChart.StableBranch}}`
            - [ ] Make sure all your changes locally are the same as remote `git reset --hard security/{{.HelmChart.StableBranch}}`
            - [ ] Push changes to origin `git push -u origin {{.HelmChart.StableBranch}}`
        {{- end}}

#### Make GitLab release public

{{range .Tags}}
- [ ] Run [`stable gitlab release`](https://gitlab.com/gitlab-org/security/gitlab-runner/-/pipelines?page=1&scope=all&ref={{.Runner.String}}) manual job
{{end}}
