{{$latest := .Tags.Latest}}
### Update upstream projects using Runner

#### [Cluster Applications](https://gitlab.com/gitlab-org/project-templates/cluster-management)

- [ ] Create branch

    ```shell
    rrhelper \
        --dry-run \
        create-branch \
        gitlab-org/project-templates/cluster-management \
        update-gitlab-runner-helm-chart-to-{{$latest.HelmChart.Major}}-{{$latest.HelmChart.Minor}}-{{$latest.HelmChart.Patch}}
    ```

- [ ] Create Merge Request

    ```shell
    rrhelper \
        --dry-run \
        create-merge-request \
        --release-checklist-issue {{.SecurityReleaseChecklistID}} \
        --replace-link-in-release-checklist-issue link_to_MR__ghc_rhcvu \
        --milestone {{$latest.Runner.Major}}.{{$latest.Runner.Minor}} \
        --runner-version {{$latest.Runner.Major}}.{{$latest.Runner.Minor}}.{{$latest.HelmChart.Patch}} \
        --helm-chart-version {{$latest.HelmChart.Major}}.{{$latest.HelmChart.Minor}}.{{$latest.HelmChart.Patch}} \
        runner-helm-chart-upgrade-in-cluster-management \
        update-gitlab-runner-helm-chart-to-{{$latest.HelmChart.Major}}-{{$latest.HelmChart.Minor}}-{{$latest.HelmChart.Patch}} \
        master \
        runner-helm-chart-upgrade-in-cluster-management \
        "feat: Update GitLab Runner Helm Chart to {{$latest.HelmChart.Major}}.{{$latest.HelmChart.Minor}}.{{$latest.HelmChart.Patch}}/{{$latest.Runner.Major}}.{{$latest.Runner.Minor}}.{{$latest.Runner.Patch}}"
    ```

- [ ] Adjust and apply the patch to the GitLab Helm Chart sources

    <details>
    <summary>See the patch draft</summary>

    - [ ] Adjust the following patch (set proper current version; set the Merge Request ID in the CHANGELOG entry file)
    and save it at `/tmp/patch.gitlab-cluster-management`.

        ```diff
        cat > /tmp/patch.gitlab-cluster-management << EOF
        diff --git a/applications/gitlab-runner/helmfile.yaml b/applications/gitlab-runner/helmfile.yaml
        index 5b97c91..7799d50 100644
        --- a/applications/gitlab-runner/helmfile.yaml
        +++ b/applications/gitlab-runner/helmfile.yaml
        @@ -6,7 +6,7 @@ releases:
         - name: runner
           namespace: gitlab-managed-apps
           chart: gitlab/gitlab-runner
        -  version: {{$latest.HelmChart.Major}}.{{$latest.HelmChart.Minor}}.{{dec $latest.HelmChart.Patch}}
        +  version: {{$latest.HelmChart.Major}}.{{$latest.HelmChart.Minor}}.{{$latest.HelmChart.Patch}}
           installed: true
           values:
             - values.yaml.gotmpl
        EOF
        ```

    - [ ] Go to your local GitLab Cluster Application working directory:

        ```shell
        git checkout master && \
            git pull && \
            git checkout update-gitlab-runner-helm-chart-to-{{$latest.HelmChart.Major}}-{{$latest.HelmChart.Minor}}-{{$latest.HelmChart.Patch}} && \
            git apply /tmp/patch.gitlab-cluster-management && \
            git add . && \
            git commit -m "feat: Update GitLab Runner Helm Chart to {{$latest.HelmChart.Major}}.{{$latest.HelmChart.Minor}}.{{$latest.HelmChart.Patch}}"
        ```

    - [ ] Push changes

        ```shell
        git push -u origin update-gitlab-runner-helm-chart-to-{{$latest.HelmChart.Major}}-{{$latest.HelmChart.Minor}}-{{$latest.HelmChart.Patch}}
        ```

    </details>
