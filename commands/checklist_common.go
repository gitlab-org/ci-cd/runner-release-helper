package commands

import (
	"fmt"
	"reflect"
	"regexp"
	"strconv"
	"strings"

	"github.com/sirupsen/logrus"
	"gopkg.in/yaml.v2"

	"gitlab.com/gitlab-org/ci-cd/runner-release-helper/gitlab"
	"gitlab.com/gitlab-org/ci-cd/runner-release-helper/helpers/console"
)

const (
	runnerProjectID    = "gitlab-org/gitlab-runner"
	helmChartProjectID = "gitlab-org/charts/gitlab-runner"

	runnerVersionFile    = "VERSION"
	helmChartVersionFile = "Chart.yaml"
)

func newChecklistCommon() checklistCommon {
	return checklistCommon{
		base:                 new(base),
		RunnerProjectID:      runnerProjectID,
		HelmChartProjectID:   helmChartProjectID,
		RunnerVersionFile:    runnerVersionFile,
		HelmChartVersionFile: helmChartVersionFile,
		RunnerMRs:            newRunnerMRsList(1),
		HelmChartMRs:         newHelmChartMRsList(1),
	}
}

type mrsListProject string

const (
	mrsListRunnerProject    mrsListProject = "runner"
	mrsListHelmChartProject mrsListProject = "helm_chart"
)

type mrsProjectSlug string

const (
	mrsRunnerProjectSlug    mrsProjectSlug = "gitlab-org/gitlab-runner"
	mrsHelmChartProjectSlug mrsProjectSlug = "gitlab-org/charts/gitlab-runner"
)

type mrsList struct {
	Project     mrsListProject
	ProjectSlug mrsProjectSlug

	RC  int
	MRs gitlab.MergeRequests
}

func newRunnerMRsList(rc int, mrs ...gitlab.MergeRequest) mrsList {
	return mrsList{Project: mrsListRunnerProject, ProjectSlug: mrsRunnerProjectSlug, RC: rc, MRs: mrs}
}

func newHelmChartMRsList(rc int, mrs ...gitlab.MergeRequest) mrsList {
	return mrsList{Project: mrsListHelmChartProject, ProjectSlug: mrsHelmChartProjectSlug, RC: rc, MRs: mrs}
}

type checklistCommon struct {
	*base

	Major int `long:"major" description:"Major version number for GitLab Runner"`
	Minor int `long:"minor" description:"Minor version number for GitLab Runner"`

	HelmChartMajor int `long:"helm-chart-major" description:"Major version number for GitLab Runner's Helm Chart'"`
	HelmChartMinor int `long:"helm-chart-minor" description:"Minor version number for GitLab Runner's Helm Chart"`

	NonInteractive bool `long:"non-interactive" description:"Don't ask, just try to work!"`
	DoNotCreate    bool `long:"do-not-create" description:"Don't create the issue, just run auto-detection"`

	RunnerProjectID    string `long:"runner-project" description:"ID of Runner's project"`
	HelmChartProjectID string `long:"helm-chart-project" description:"ID of Runner Helm Chart's project"`

	RunnerVersionFile    string `long:"runner-version-file" description:"Path of Runner's VERSION file; relative to the repository"`
	HelmChartVersionFile string `long:"helm-chart-version-file" description:"Path of Runner Helm Chart's VERSION file; relative to the repository'"`

	RunnerMRs    mrsList
	HelmChartMRs mrsList
}

type valueQuestion struct {
	prompt    string
	value     interface{}
	requester console.InputRequesterFactory
}

func (c *checklistCommon) autodetect(autodetectors ...func() error) error {
	defaultAutodetectors := []func() error{
		c.detectRunnerVersion,
		c.detectHelmChartVersion,
	}
	autodetectors = append(defaultAutodetectors, autodetectors...)

	for _, autodetector := range autodetectors {
		err := autodetector()
		if err != nil {
			return err
		}
	}

	questions := []valueQuestion{
		{prompt: "Major version number", value: &c.Major, requester: console.NewIntRequester},
		{prompt: "Minor version number", value: &c.Minor, requester: console.NewIntRequester},
		{prompt: "Helm Chart major version number", value: &c.HelmChartMajor, requester: console.NewIntRequester},
		{prompt: "Helm Chart minor version number", value: &c.HelmChartMinor, requester: console.NewIntRequester},
	}

	err := c.askForValues(questions)
	if err != nil {
		return fmt.Errorf("error while performing value questions: %v", err)
	}

	return nil
}

func (c *checklistCommon) askForValues(questions []valueQuestion) error {
	for _, question := range questions {
		requester, err := question.requester(c.NonInteractive, question.value)
		if err != nil {
			return err
		}

		err = requester.Ask(question.prompt)
		if err != nil {
			return err
		}
	}

	return nil
}

func (c *checklistCommon) detectRunnerVersion() error {
	if c.skipAutodetectionWhenPresent(c.Major, c.Minor) {
		return nil
	}

	logrus.Print("Auto-detecting Runner version...")

	params := gitlab.ReadRemoteFileParams{
		ProjectID: c.RunnerProjectID,
		Path:      c.RunnerVersionFile,
		Ref:       "main",
	}
	content, err := c.client.ReadRemoteFile(params)
	if err != nil {
		return err
	}

	logrus.Printf("Found: %s", content)

	detectedVersion := strings.Split(string(content), ".")
	if len(detectedVersion) < 2 {
		logrus.Warning("couldn't parse version string")

		return nil
	}

	c.Major, err = strconv.Atoi(detectedVersion[0])
	if err != nil {
		return err
	}

	c.Minor, err = strconv.Atoi(detectedVersion[1])
	if err != nil {
		return err
	}

	return nil
}

func (c *checklistCommon) skipAutodetectionWhenPresent(objects ...interface{}) bool {
	for _, object := range objects {
		if !isEmpty(object) {
			return true
		}
	}

	return false
}

// isEmpty implementation was taken from the awesome testify project:
//   https://github.com/stretchr/testify/blob/v1.3.0/assert/assertions.go#L468-494
//
// The original code is licensed under MIT license, and the copyright
// goes to Mat Ryer and Tyler Bunnell
func isEmpty(object interface{}) bool {
	if object == nil {
		return false
	}

	value := reflect.ValueOf(object)

	switch value.Kind() {
	case reflect.Array, reflect.Chan, reflect.Map, reflect.Slice:
		return value.Len() == 0
	case reflect.Ptr:
		if value.IsNil() {
			return true
		}

		return isEmpty(value.Elem().Interface())
	default:
		zero := reflect.Zero(value.Type())
		return reflect.DeepEqual(object, zero.Interface())
	}
}

type helmChartData struct {
	Version string `yaml:"version"`
}

func (c *checklistCommon) detectHelmChartVersion() error {
	if c.skipAutodetectionWhenPresent(c.HelmChartMajor, c.HelmChartMinor) {
		return nil
	}

	logrus.Print("Auto-detecting Runner's Helm Chart version...")

	params := gitlab.ReadRemoteFileParams{
		ProjectID: c.HelmChartProjectID,
		Path:      c.HelmChartVersionFile,
		Ref:       "main",
	}
	content, err := c.client.ReadRemoteFile(params)
	if err != nil {
		return err
	}

	helmChartData := new(helmChartData)
	err = yaml.Unmarshal(content, helmChartData)
	if err != nil {
		return fmt.Errorf("error while parsing Helm Chart version file: %v", err)
	}

	logrus.Printf("Found: %s", helmChartData.Version)

	return c.setHelmChartVersion(*helmChartData)
}

func (c *checklistCommon) setHelmChartVersion(helmChartData helmChartData) error {
	var err error
	versionRx := regexp.MustCompile(`([0-9]+)\.([0-9]+)\.([0-9]+)`)
	versions := versionRx.FindAllStringSubmatch(helmChartData.Version, -1)

	if len(versions) != 1 {
		logrus.Warning("couldn't parse version string")

		return nil
	}

	c.HelmChartMajor, err = strconv.Atoi(versions[0][1])
	if err != nil {
		return err
	}

	c.HelmChartMinor, err = strconv.Atoi(versions[0][2])
	if err != nil {
		return err
	}

	return nil
}

func (c *checklistCommon) guardCreate(callback func() error, fallback func() error) error {
	if c.DoNotCreate {
		var err error
		if fallback != nil {
			err = fallback()
		}

		logrus.Warning("skipping issue creation due to DoNotCreate option")

		return err
	}

	return callback()
}
