package commands

func newRCCommon() rcCommon {
	return rcCommon{
		checklistCommon: newChecklistCommon(),
	}
}

type rcCommon struct {
	checklistCommon

	ReleaseChecklistIssueID int `argument:"release checklist issue ID"`
	RC                      int `argument:"rc number"`
}
