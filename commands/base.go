package commands

import (
	"fmt"

	"gitlab.com/gitlab-org/ci-cd/runner-release-helper/gitlab"
)

type base struct {
	client gitlab.Client
}

func (c *base) SetGitLabClient(client gitlab.Client) {
	c.client = client
}

func (c *base) checkGitLabClient() error {
	if c.client == nil {
		return fmt.Errorf("GitLab Client not provided")
	}

	return nil
}
