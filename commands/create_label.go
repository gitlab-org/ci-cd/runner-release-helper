package commands

import (
	"github.com/sirupsen/logrus"

	"gitlab.com/gitlab-org/ci-cd/runner-release-helper/app"
	"gitlab.com/gitlab-org/ci-cd/runner-release-helper/gitlab"
)

func NewCreateLabel() *CreateLabel {
	return &CreateLabel{
		base: new(base),
	}
}

type CreateLabel struct {
	*base

	ProjectID   string `argument:"project_id"`
	Name        string `argument:"name"`
	Description string `argument:"description"`
	Color       string `argument:"color"`
}

func (c *CreateLabel) Execute(appCtx app.Context) error {
	err := c.checkGitLabClient()
	if err != nil {
		return err
	}

	params := gitlab.CreateLabelParams{
		ProjectID:   c.ProjectID,
		Name:        c.Name,
		Description: c.Description,
		Color:       c.Color,
	}

	label, err := c.client.CreateLabel(params)
	if err != nil {
		return err
	}

	logrus.Printf("Successfully created label %q", label.Name)

	return nil
}
