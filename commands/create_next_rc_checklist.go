package commands

import (
	"fmt"
	"strings"

	"github.com/sirupsen/logrus"

	"gitlab.com/gitlab-org/ci-cd/runner-release-helper/app"
	"gitlab.com/gitlab-org/ci-cd/runner-release-helper/gitlab"
	"gitlab.com/gitlab-org/ci-cd/runner-release-helper/helpers/content"
	"gitlab.com/gitlab-org/ci-cd/runner-release-helper/helpers/template"
)

const (
	nextRCStartLineMark = "<!-- next_rc -->"
	nextRCEndLineMark   = "<!-- next_rc_end -->"
)

func NewCreateNextRCChecklist() *CreateNextRCChecklist {
	return &CreateNextRCChecklist{
		rcCommon: newRCCommon(),
	}
}

type CreateNextRCChecklist struct {
	rcCommon
}

func (c *CreateNextRCChecklist) Execute(appCtx app.Context) error {
	err := c.checkGitLabClient()
	if err != nil {
		return err
	}

	if c.RC < 2 {
		return fmt.Errorf("this command can be only used to create RC2 and following")
	}

	err = c.autodetect()
	if err != nil {
		return err
	}

	c.RunnerMRs = newRunnerMRsList(c.RC)
	c.HelmChartMRs = newHelmChartMRsList(c.RC)

	return c.create()
}

func (c *CreateNextRCChecklist) create() error {
	readIssueParams := gitlab.ReadIssueParams{
		ProjectID: c.RunnerProjectID,
		IID:       c.ReleaseChecklistIssueID,
	}
	issue, err := c.client.ReadIssue(readIssueParams)
	if err != nil {
		return err
	}

	if strings.Contains(issue.Description, fmt.Sprintf("this is rc%d part", c.RC)) {
		return fmt.Errorf("RC%d already exists", c.RC)
	}

	checklist, err := c.prepareRCChecklist()
	if err != nil {
		return err
	}

	replacer := content.NewBlockLineReplacer(nextRCStartLineMark, nextRCEndLineMark, issue.Description, checklist)
	newDescription, err := replacer.Replace()
	if err != nil {
		return fmt.Errorf("error while replacing issue content: %v", err)
	}

	updateIssueFn := func() error {
		updateIssueParams := gitlab.UpdateIssueParams{
			ProjectID:   c.RunnerProjectID,
			IID:         issue.IID,
			Title:       issue.Title,
			Description: newDescription,
		}
		issue, err = c.client.UpdateIssue(updateIssueParams)
		if err != nil {
			return err
		}

		logrus.Printf("Successfully updated issue %q at %s#v%d%d0-rc%d-release", issue.Title, issue.WebURL, c.Major, c.Minor, c.RC)

		return nil
	}

	printIssueFn := func() error {
		fmt.Println(newDescription)

		return nil
	}

	return c.guardCreate(updateIssueFn, printIssueFn)
}

func (c *CreateNextRCChecklist) prepareRCChecklist() (string, error) {
	tpl, err := template.Load(template.TypeIssue, "release-checklist-parts/rc-checklist")
	if err != nil {
		return "", err
	}

	_, err = tpl.LoadPart("rc-mrs-list")
	if err != nil {
		return "", err
	}

	return tpl.Execute(c)
}
