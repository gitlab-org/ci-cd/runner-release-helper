package commands

import (
	"strings"

	"github.com/sirupsen/logrus"

	"gitlab.com/gitlab-org/ci-cd/runner-release-helper/app"
	"gitlab.com/gitlab-org/ci-cd/runner-release-helper/gitlab"
	"gitlab.com/gitlab-org/ci-cd/runner-release-helper/helpers/template"
)

func NewCreateMergeRequest() *CreateMergeRequest {
	return &CreateMergeRequest{
		base:            new(base),
		RunnerProjectID: runnerProjectID,
	}
}

type CreateMergeRequest struct {
	*base

	ProjectID    string `argument:"project_id"`
	SourceBranch string `argument:"source_branch"`
	TargetBranch string `argument:"target_branch"`
	TemplateName string `argument:"template_name"`
	Title        string `argument:"title"`

	RunnerVersion           string `long:"runner-version" description:"Version number of GitLab Runner"`
	HelmChartVersion        string `long:"helm-chart-version" description:"Version number of GitLab Runner's Helm Chart'"`
	Milestone               string `long:"milestone" description:"Related milestone"`
	ReleaseChecklistIssueID int    `long:"release-checklist-issue" description:"ID of the Release Checklist issue"`

	ReplaceLinkInReleaseChecklistIssue string `long:"replace-link-in-release-checklist-issue" description:"If the value is present, it will be replaced in the Release Checklist issue by the WebURL of created MR"`

	RunnerProjectID string `long:"runner-project" description:"ID of Runner's project"`
}

func (c *CreateMergeRequest) Execute(appCtx app.Context) error {
	err := c.checkGitLabClient()
	if err != nil {
		return err
	}

	content, err := c.prepareContent()
	if err != nil {
		return err
	}

	params := gitlab.CreateMergeRequestParams{
		ProjectID:    c.ProjectID,
		Title:        c.Title,
		Description:  content,
		SourceBranch: c.SourceBranch,
		TargetBranch: c.TargetBranch,
		Remove:       true,
	}

	mergeRequest, err := c.client.CreateMergeRequest(params)
	if err != nil {
		return err
	}

	logrus.Printf("Successfully created Merge Request %q at %s", mergeRequest.Title, mergeRequest.WebURL)

	return c.updateReleaseChecklistIssueWithMRWebURL(mergeRequest)
}

func (c *CreateMergeRequest) prepareContent() (string, error) {
	tpl, err := template.Load(template.TypeMergeRequest, c.TemplateName)
	if err != nil {
		return "", err
	}

	return tpl.Execute(c)
}

func (c *CreateMergeRequest) updateReleaseChecklistIssueWithMRWebURL(mergeRequest gitlab.MergeRequest) error {
	if c.ReleaseChecklistIssueID == 0 || c.ReplaceLinkInReleaseChecklistIssue == "" {
		logrus.Warning("ReleaseChecklistIssueID or ReplaceLinkInReleaseChecklistIssue undefined; skipping issue update")
		return nil
	}

	readIssueParams := gitlab.ReadIssueParams{
		ProjectID: c.RunnerProjectID,
		IID:       c.ReleaseChecklistIssueID,
	}
	issue, err := c.client.ReadIssue(readIssueParams)
	if err != nil {
		return err
	}

	updateIssueParams := gitlab.UpdateIssueParams{
		ProjectID:   c.RunnerProjectID,
		IID:         issue.IID,
		Title:       issue.Title,
		Description: strings.Replace(issue.Description, c.ReplaceLinkInReleaseChecklistIssue, mergeRequest.WebURL, 1),
	}
	issue, err = c.client.UpdateIssue(updateIssueParams)
	if err != nil {
		return err
	}

	logrus.Printf("Successfully updated issue %q at %s", issue.Title, issue.WebURL)

	return nil
}
