package commands

import (
	"fmt"

	"github.com/sirupsen/logrus"

	"gitlab.com/gitlab-org/ci-cd/runner-release-helper/app"
	"gitlab.com/gitlab-org/ci-cd/runner-release-helper/gitlab"
	"gitlab.com/gitlab-org/ci-cd/runner-release-helper/helpers/content"
	"gitlab.com/gitlab-org/ci-cd/runner-release-helper/helpers/template"
)

const (
	pickMRState = "merged"

	mrsListStartLineMarkPattern = "<!-- %s_pick_mrs_list_for_rc%d -->"
	mrsListEndLineMarkPattern   = "<!-- %s_pick_mrs_list_for_rc%d_end -->"
)

func NewListPickMrsForRC() *ListPickMrsForRC {
	return &ListPickMrsForRC{
		rcCommon: newRCCommon(),
	}
}

type ListPickMrsForRC struct {
	rcCommon
}

func (c *ListPickMrsForRC) Execute(appCtx app.Context) error {
	err := c.checkGitLabClient()
	if err != nil {
		return err
	}

	if c.RC < 2 {
		return fmt.Errorf("this command can be only used to create RC2 and following")
	}

	err = c.autodetect()
	if err != nil {
		return err
	}

	return c.create()
}

func (c *ListPickMrsForRC) create() error {
	readIssueParams := gitlab.ReadIssueParams{
		ProjectID: c.RunnerProjectID,
		IID:       c.ReleaseChecklistIssueID,
	}
	issue, err := c.client.ReadIssue(readIssueParams)
	if err != nil {
		return err
	}

	newDescription, err := c.prepareMRsList(issue.Description, mrsListRunnerProject, mrsRunnerProjectSlug, runnerProjectID, fmt.Sprintf("Pick into %d.%d", c.Major, c.Minor))
	if err != nil {
		return err
	}

	newDescription, err = c.prepareMRsList(newDescription, mrsListHelmChartProject, mrsHelmChartProjectSlug, helmChartProjectID, fmt.Sprintf("Pick into %d.%d", c.HelmChartMajor, c.HelmChartMinor))
	if err != nil {
		return err
	}

	updateIssueFn := func() error {
		updateIssueParams := gitlab.UpdateIssueParams{
			ProjectID:   c.RunnerProjectID,
			IID:         issue.IID,
			Title:       issue.Title,
			Description: newDescription,
		}
		issue, err = c.client.UpdateIssue(updateIssueParams)
		if err != nil {
			return err
		}

		logrus.Printf("Successfully updated issue %q at %s#pick-into-stable-branch-for-rc%d-phase", issue.Title, issue.WebURL, c.RC)

		return nil
	}

	printIssueFn := func() error {
		fmt.Println(newDescription)

		return nil
	}

	return c.guardCreate(updateIssueFn, printIssueFn)
}

func (c *ListPickMrsForRC) prepareMRsList(description string, project mrsListProject, projectSlug mrsProjectSlug, projectID string, label string) (string, error) {
	runnerMRsOptions := gitlab.ListMergeRequestsParams{
		ProjectID: projectID,
		Label:     []string{label},
		State:     pickMRState,
	}

	data := mrsList{Project: project, ProjectSlug: projectSlug, RC: c.RC - 1}

	var err error
	data.MRs, err = c.client.ListMergeRequests(runnerMRsOptions)
	if err != nil {
		return "", err
	}

	tpl, err := template.Load(template.TypeIssue, "release-checklist-parts/rc-checklist-parts/rc-mrs-list")
	if err != nil {
		return "", err
	}

	aaa, err := tpl.Execute(data)
	if err != nil {
		return "", err
	}

	startLineMark := fmt.Sprintf(mrsListStartLineMarkPattern, project, c.RC)
	endLineMark := fmt.Sprintf(mrsListEndLineMarkPattern, project, c.RC)

	replacer := content.NewBlockLineReplacer(startLineMark, endLineMark, description, aaa)
	newDescription, err := replacer.Replace()
	if err != nil {
		return "", fmt.Errorf("error while replacing issue content: %v", err)
	}

	return newDescription, nil
}
